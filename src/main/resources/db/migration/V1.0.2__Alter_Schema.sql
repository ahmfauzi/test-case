
ALTER TABLE
    reports
add
    constraint FK_report_subs foreign key (subscriber_id) references subscribers;