INSERT INTO users (username, name, password, email, user_level) VALUES
    ('jun.misugi', 'Jun Misugi', '$2a$12$b3Vof7Fu3zbPi8dgyFPoF.hzN1wMamMDqOtQ7A4rnz3ymJDJMYez6', 'misugi@yopmail.com', 'SUPER_ADMIN'),
    ('saka', 'Bukayo Saka', '$2a$12$b3Vof7Fu3zbPi8dgyFPoF.hzN1wMamMDqOtQ7A4rnz3ymJDJMYez6', 'saka@yopmail.com', 'ADMIN');

INSERT INTO subscribers (name, email, phone, methode_broadcast) VALUES
    ('Alexandre Lacazette', 'alex.lacazette@yopmail.com', '08182128128192', 'SMS'),
    ('E. Smith Rowe', 'emile.rowe@yopmail.com', '08182128128191', 'EMAIL');

