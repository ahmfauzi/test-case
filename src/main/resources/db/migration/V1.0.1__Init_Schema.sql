
CREATE TABLE users (
    id serial NOT NULL primary key,
    name varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    username varchar(255) NOT NULL,
    login_counter integer NOT NULL default 0,
    last_login timestamptz,
    user_level varchar(15) NOT NULL,
    created_at timestamptz NOT NULL default timezone('Asia/Jakarta', now()),
    updated_at timestamptz
);

CREATE TABLE subscribers (
    id serial NOT NULL primary key,
    name varchar(255) NOT NULL,
    email varchar(150) NOT NULL,
    phone varchar(30) NOT NULL,
    last_broadcast timestamptz,
    methode_broadcast varchar(15) NOT NULL,
    created_at timestamptz NOT NULL default timezone('Asia/Jakarta', now()),
    updated_at timestamptz
);

CREATE TABLE reports(
    id serial NOT NULL primary key,
    subscriber_id integer NOT NULL,
    channel varchar(15) NOT NULL,
    email varchar(50) NOT NULL,
    phone varchar(50) NOT NULL,
    status varchar(50) NOT NULL,
    created_at timestamptz NOT NULL default timezone('Asia/Jakarta', now())
);