package com.mubarok.testcase.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseResponse {
    private Object data = null;
    private String message = "";
    private boolean success = false;
}
