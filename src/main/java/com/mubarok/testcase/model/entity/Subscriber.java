package com.mubarok.testcase.model.entity;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.Timezone;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Table(name = "subscribers")
@Data
public class Subscriber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    private String email;

    @NotBlank
    private String phone;

    @NotNull
    @Enumerated(EnumType.STRING)
    private BroadcastType methodeBroadcast;

    private ZonedDateTime lastBroadcast;

    private ZonedDateTime createdAt = ZonedDateTime.now(Timezone.AsiaJakarta);

    private ZonedDateTime updatedAt;

}
