package com.mubarok.testcase.model.entity;

import com.mubarok.testcase.constants.UserLevel;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Data
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    private String email;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    @NotBlank
    private ZonedDateTime lastLogin;

    @NotNull
    private int loginCounter;

    @NotNull
    @Enumerated(EnumType.STRING)
    private UserLevel userLevel;
}
