package com.mubarok.testcase.model.entity;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.ReportStatus;
import com.mubarok.testcase.constants.Timezone;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Table(name = "reports")
@Data
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "subscriber_id")
    private Subscriber subscriber;

    @NotNull
    @Enumerated(EnumType.STRING)
    private BroadcastType channel;

    @NotBlank
    private String phone;

    @NotBlank
    private String email;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ReportStatus status;

    private ZonedDateTime createdAt = ZonedDateTime.now(Timezone.AsiaJakarta);
}
