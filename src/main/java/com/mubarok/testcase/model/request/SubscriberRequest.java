package com.mubarok.testcase.model.request;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.validation.EnumValidator;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class SubscriberRequest {
    @NotBlank(message = "name is requiered.")
    private String name;

    @NotBlank(message = "phone is requiered.")
    private String phone;

    @NotBlank(message = "email is requiered.")
    private String email;

    @EnumValidator(enumClass = BroadcastType.class, message = "bcType is not valid")
    @NotBlank(message = "bcType is required.")
    private String bcType;
}
