package com.mubarok.testcase.usecase.interfaces;

import com.mubarok.testcase.model.request.LoginRequest;
import com.mubarok.testcase.model.response.BaseResponse;

public interface AuthService {
    public BaseResponse login(LoginRequest request);
}
