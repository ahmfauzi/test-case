package com.mubarok.testcase.usecase.interfaces;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.ReportStatus;
import com.mubarok.testcase.model.response.BaseResponse;

public interface ReportService {
    BaseResponse all(String startDate, String endDate, BroadcastType type, ReportStatus status);
}
