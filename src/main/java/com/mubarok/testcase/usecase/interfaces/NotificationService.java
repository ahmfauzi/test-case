package com.mubarok.testcase.usecase.interfaces;

import org.springframework.scheduling.annotation.Async;

public interface NotificationService {
    @Async
    void send();
}
