package com.mubarok.testcase.usecase.interfaces;

import com.mubarok.testcase.model.request.SubscriberRequest;
import com.mubarok.testcase.model.response.BaseResponse;

public interface SubscriberService {
    BaseResponse add(SubscriberRequest request);

    BaseResponse all();

    BaseResponse edit(Integer id, SubscriberRequest request);

    BaseResponse show(Integer id);

    BaseResponse delete(Integer id);
}
