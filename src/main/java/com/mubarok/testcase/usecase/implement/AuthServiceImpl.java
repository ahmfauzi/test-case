package com.mubarok.testcase.usecase.implement;

import com.mubarok.testcase.authentication.JwtTokenUtil;
import com.mubarok.testcase.model.request.LoginRequest;
import com.mubarok.testcase.model.response.BaseResponse;
import com.mubarok.testcase.model.response.LoginResponse;
import com.mubarok.testcase.usecase.interfaces.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Override
    public BaseResponse login(LoginRequest request) {
        var response = new BaseResponse();
        try {
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getUsername(),
                            request.getPassword()
                    )
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            final String token = jwtTokenUtil.generateToken(authentication);

            var loginResp = new LoginResponse();
            loginResp.setAccessToken(token);

            response.setData(loginResp);
            response.setSuccess(true);
            return response;

        } catch (Exception e) {
            log.error("[ERROR] => {}", e.getMessage());
            response.setMessage("Login Failed.");
            return response;
        }
    }
}
