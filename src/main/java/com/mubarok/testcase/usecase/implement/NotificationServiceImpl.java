package com.mubarok.testcase.usecase.implement;

import com.mubarok.testcase.configuration.TwilioConfiguration;
import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.ReportStatus;
import com.mubarok.testcase.constants.Timezone;
import com.mubarok.testcase.model.entity.Report;
import com.mubarok.testcase.model.entity.Subscriber;
import com.mubarok.testcase.repository.ReportRepository;
import com.mubarok.testcase.repository.SubscriberRepository;
import com.mubarok.testcase.usecase.interfaces.NotificationService;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.time.ZonedDateTime;

@Transactional
@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {

    private final TwilioConfiguration twilioConfiguration;

    @Value("${spring.mail.host}")
    String HOST;

    @Value("${spring.mail.port}")
    int PORT;

    @Value("${spring.mail.username}")
    String username;

    @Value("${spring.mail.password}")
    String password;

    @Value("${config.broadcast.body}")
    String text;

    @Autowired
    SubscriberRepository subsRepo;

    @Autowired
    ReportRepository reportRepo;

    @Autowired
    public NotificationServiceImpl(TwilioConfiguration twilioConfiguration) {
        this.twilioConfiguration = twilioConfiguration;
    }

    @Override
    @Async
    public void send() {
        var subscribers = subsRepo.findAll();
        if (subscribers.isEmpty()) {
            return;
        }

        log.info("[NOTIFICATION] => Start sending... ");

        subscribers.forEach(s -> {
            s.setLastBroadcast(ZonedDateTime.now(Timezone.AsiaJakarta));
            subsRepo.save(s);

            if (s.getMethodeBroadcast() == BroadcastType.EMAIL) {
                String body = String.format(text, s.getName());
                sendMail(s, body);
            }

            if (s.getMethodeBroadcast() == BroadcastType.SMS) {
                String body = String.format(text, s.getName());
                sendSMS(s, body);
            }
        });

    }

    void sendMail(Subscriber s, String body) {
        var report = new Report();
        try {
            report.setChannel(BroadcastType.EMAIL);
            report.setEmail(s.getEmail());
            report.setSubscriber(s);
            report.setPhone(s.getPhone());

            boolean SSL_FLAG = true;

            HtmlEmail email = new HtmlEmail();
            email.setHostName(HOST);
            email.setSmtpPort(PORT);
            email.setAuthenticator(new DefaultAuthenticator(username, password));
            email.setFrom(username, "APP");
            email.setSubject("Notification");
            email.setSSLOnConnect(SSL_FLAG);
            email.setHtmlMsg(body);
            email.addTo(s.getEmail());
            email.send();

            report.setStatus(ReportStatus.SUCCESS);
            log.info("[SUCCESS] Send email to : {} ", s.getEmail());

        } catch (Exception e) {
            report.setStatus(ReportStatus.FAILED);
            log.error("[ERROR] Send email error to : {} | {}", s.getEmail(), e.getMessage());
        }

        reportRepo.save(report);
    }

    void sendSMS(Subscriber s, String body) {
        var report = new Report();
        try {
            report.setChannel(BroadcastType.SMS);
            report.setEmail(s.getEmail());
            report.setSubscriber(s);
            report.setPhone(s.getPhone());

            PhoneNumber to = new PhoneNumber(s.getPhone());
            PhoneNumber from = new PhoneNumber(twilioConfiguration.getTrialNumber());
            MessageCreator creator = Message.creator(to, from, body);
            creator.create();

            report.setStatus(ReportStatus.SUCCESS);

            log.info("[SUCCESS] Send message to : {} ", s.getPhone());
        } catch (Exception e) {
            report.setStatus(ReportStatus.FAILED);
            log.error("[ERROR] Error send message to : {} ", s.getPhone());
        }

        reportRepo.save(report);
    }
}
