package com.mubarok.testcase.usecase.implement;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.Timezone;
import com.mubarok.testcase.model.entity.Subscriber;
import com.mubarok.testcase.model.request.SubscriberRequest;
import com.mubarok.testcase.model.response.BaseResponse;
import com.mubarok.testcase.repository.ReportRepository;
import com.mubarok.testcase.repository.SubscriberRepository;
import com.mubarok.testcase.usecase.interfaces.SubscriberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;

@Transactional
@Service
@Slf4j
public class SubscriberServiceImpl implements SubscriberService {

    @Autowired
    SubscriberRepository subsRepo;

    @Autowired
    ReportRepository reportRepo;

    @Override
    public BaseResponse add(SubscriberRequest request) {
        var response = new BaseResponse();
        try {

            var subscriber = new Subscriber();
            subscriber.setEmail(request.getEmail());
            subscriber.setMethodeBroadcast(BroadcastType.valueOf(request.getBcType()));
            subscriber.setName(request.getName());
            subscriber.setPhone(request.getPhone());

            subsRepo.save(subscriber);

            response.setMessage("Successfully saved");
            response.setSuccess(true);
            return response;
        } catch (Exception e) {

            response.setMessage("Fail to save");
            return response;
        }
    }

    @Override
    public BaseResponse all() {
        var response = new BaseResponse();
        var subs = subsRepo.findAllByOrderByIdAsc();
        if (subs.isEmpty()) {
            response.setMessage("Data not found.");
            return response;
        }

        response.setData(subs);
        response.setSuccess(true);
        return response;
    }

    @Override
    public BaseResponse edit(Integer id, SubscriberRequest request) {
        var response = new BaseResponse();
        try {
            var subs = subsRepo.findById(id);
            if (subs.isEmpty()) {
                response.setMessage("Data not found");
                return response;
            }

            var subscriber = subs.get();
            subscriber.setEmail(request.getEmail());
            subscriber.setMethodeBroadcast(BroadcastType.valueOf(request.getBcType()));
            subscriber.setName(request.getName());
            subscriber.setPhone(request.getPhone());
            subscriber.setUpdatedAt(ZonedDateTime.now(Timezone.AsiaJakarta));

            subsRepo.save(subscriber);

            response.setMessage("Update success.");
            response.setSuccess(true);
            return response;

        } catch (Exception e) {
            log.error("[ERROR] => {}", e.getMessage());
            response.setMessage("Update failed.");
            return response;
        }
    }

    @Override
    public BaseResponse show(Integer id) {
        var response = new BaseResponse();
        var subs = subsRepo.findById(id);
        if (subs.isEmpty()) {
            response.setMessage("Data not found");
            return response;
        }

        var subscriber = subs.get();

        response.setData(subscriber);
        response.setSuccess(true);
        return response;
    }

    @Override
    public BaseResponse delete(Integer id) {
        var response = new BaseResponse();
        try {
            var subs = subsRepo.findById(id);
            if (subs.isEmpty()) {
                response.setMessage("Data not found.");
                return response;
            }
            var subscriber = subs.get();
            var report = reportRepo.findBySubscriberId(subscriber.getId());
            if (report != null) {
                response.setMessage("Subscriber is used in table reports.");
                return response;
            }

            subsRepo.delete(subscriber);

            response.setMessage("Delete success.");
            response.setSuccess(true);
            return response;

        } catch (Exception e) {
            response.setMessage("Fail to delete.");
            return response;
        }
    }
}
