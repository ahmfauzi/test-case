package com.mubarok.testcase.usecase.implement;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.ReportStatus;
import com.mubarok.testcase.model.response.BaseResponse;
import com.mubarok.testcase.repository.ReportRepository;
import com.mubarok.testcase.usecase.interfaces.ReportService;
import com.mubarok.testcase.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;

@Transactional
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    ReportRepository reportRepo;

    @Override
    public BaseResponse all(String startDate, String endDate, BroadcastType type, ReportStatus status) {
        var response = new BaseResponse();

        ZonedDateTime start = DateUtil.stringDateToZonedDate(startDate, "yyyy-MM-dd"),
                finish = DateUtil.stringDateToZonedDate(endDate, "yyyy-MM-dd");

        if (start == null || endDate == null) {
            response.setMessage("Start date or end date is false.");
            return response;
        }

        if (start.isAfter(finish)) {
            var message = String.format("%s > %s", startDate, endDate);
            response.setMessage(message);
            return response;
        }

        var reports = reportRepo.findByCreatedAtBetweenAndChannelAndStatus(start, finish, type, status);

        response.setData(reports);
        response.setSuccess(true);
        return response;
    }

}
