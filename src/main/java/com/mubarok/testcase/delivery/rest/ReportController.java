package com.mubarok.testcase.delivery.rest;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.ReportStatus;
import com.mubarok.testcase.usecase.interfaces.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/report")
public class ReportController {

    @Autowired
    ReportService reportService;

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping
    public ResponseEntity<?> all(@RequestParam String startDate, @RequestParam String endDate,
                                 @RequestParam BroadcastType type, @RequestParam ReportStatus status) {
        var response = reportService.all(startDate, endDate, type, status);
        return ResponseEntity.ok(response);
    }

}
