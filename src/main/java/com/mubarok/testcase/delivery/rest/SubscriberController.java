package com.mubarok.testcase.delivery.rest;

import com.mubarok.testcase.model.request.SubscriberRequest;
import com.mubarok.testcase.usecase.interfaces.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/subscriber")
public class SubscriberController {

    @Autowired
    SubscriberService subsService;

    @GetMapping
    public ResponseEntity<?> all() {
        var response = subsService.all();
        return ResponseEntity.ok(response);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> show(@PathVariable Integer id) {
        var response = subsService.show(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestBody SubscriberRequest request) {
        var response = subsService.add(request);
        return ResponseEntity.ok(response);
    }

    @PutMapping("{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @Valid @RequestBody SubscriberRequest request) {
        var response = subsService.edit(id, request);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        var response = subsService.delete(id);
        return ResponseEntity.ok(response);
    }
}
