package com.mubarok.testcase.delivery.scheduler;

import com.mubarok.testcase.usecase.interfaces.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class NotificationSchedule {

    @Autowired
    private NotificationService notifService;

    @Scheduled(cron = "${config.cron.expression}")
    public void sendBroadcast() {
        notifService.send();
    }

}
