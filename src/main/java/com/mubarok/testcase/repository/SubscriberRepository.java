package com.mubarok.testcase.repository;

import com.mubarok.testcase.model.entity.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubscriberRepository extends JpaRepository<Subscriber, Integer> {
    List<Subscriber> findAllByOrderByIdAsc();
}
