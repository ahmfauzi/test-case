package com.mubarok.testcase.repository;

import com.mubarok.testcase.constants.BroadcastType;
import com.mubarok.testcase.constants.ReportStatus;
import com.mubarok.testcase.model.entity.Report;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.ZonedDateTime;
import java.util.List;

public interface ReportRepository extends JpaRepository<Report, Integer> {
    List<Report> findByCreatedAtBetweenAndChannelAndStatus
            (ZonedDateTime start, ZonedDateTime end, BroadcastType channel, ReportStatus status);

    Report findBySubscriberId(Integer id);
}
