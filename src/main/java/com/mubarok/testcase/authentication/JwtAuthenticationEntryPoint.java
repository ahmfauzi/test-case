package com.mubarok.testcase.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mubarok.testcase.model.response.BaseResponse;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        Exception exception = (Exception) request.getAttribute("exception");

        var message = exception == null ? authException.getMessage() : exception.getMessage();

        var resp = new BaseResponse();
        resp.setMessage(message);

        byte[] body = new ObjectMapper().writeValueAsBytes(resp);
        response.getOutputStream().write(body);
    }
}
