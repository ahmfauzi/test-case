package com.mubarok.testcase.exception;

import com.mubarok.testcase.model.response.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.ArrayList;

@RestControllerAdvice
@Slf4j
public class RequestExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public BaseResponse handleValidationExceptions(MethodArgumentNotValidException ex) {
        var response = new BaseResponse();
        var errors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = "- " + error.getDefaultMessage();
            errors.add(errorMessage);
        });

        response.setSuccess(false);
        response.setMessage("Bad Request, because request body is wrong.");
        response.setData(errors);
        return response;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            HttpMessageNotReadableException.class,
            MethodArgumentTypeMismatchException.class,
            MissingServletRequestParameterException.class,
            TypeMismatchException.class
    })
    public BaseResponse handleBadRequestExceptions(Exception e) {
        var response = new BaseResponse();

        response.setSuccess(false);
        response.setMessage("Bad Request.");
        return response;
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public BaseResponse handleMethodNotAllowed(Exception e) {
        var response = new BaseResponse();

        response.setSuccess(false);
        response.setMessage("Method not allowed.");
        return response;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public BaseResponse handleNoHandlerFound(NoHandlerFoundException e, WebRequest request) {
        var response = new BaseResponse();

        response.setSuccess(false);
        response.setMessage("Page not found.");
        return response;
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(AccessDeniedException.class)
    public BaseResponse handleAccessDeniedExceptions(AccessDeniedException ex) {
        var response = new BaseResponse();
        response.setSuccess(false);
        response.setMessage("Access denied.");
        return response;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({
            HttpMessageNotWritableException.class,
            ConversionNotSupportedException.class,
            Exception.class
    })
    public BaseResponse handleInternalErrorExceptions(Exception ex) {
        var response = new BaseResponse();
        ex.printStackTrace();
        response.setSuccess(false);
        response.setMessage("Internal server error. Try again later!");

        log.error("[Internal-Error] => {}", ex.getMessage());
        return response;
    }

}
