package com.mubarok.testcase.constants;

public enum BroadcastType {
    SMS, EMAIL
}
