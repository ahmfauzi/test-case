package com.mubarok.testcase.constants;

public enum UserLevel {
    ADMIN, SUPER_ADMIN
}
