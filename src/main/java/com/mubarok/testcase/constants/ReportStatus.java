package com.mubarok.testcase.constants;

public enum ReportStatus {
    SUCCESS, FAILED
}
