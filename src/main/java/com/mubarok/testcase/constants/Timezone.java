package com.mubarok.testcase.constants;

import java.time.ZoneId;

public class Timezone {
    public static final ZoneId AsiaJakarta = ZoneId.of("Asia/Jakarta");
}
