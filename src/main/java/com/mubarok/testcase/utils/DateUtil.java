package com.mubarok.testcase.utils;

import com.mubarok.testcase.constants.Timezone;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;

@Slf4j
public class DateUtil {
    public static ZonedDateTime stringDateToZonedDate(String stringDate, String format) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            Date date = formatter.parse(stringDate);

            return ZonedDateTime.ofInstant(date.toInstant(), Timezone.AsiaJakarta);
        } catch (Exception e) {
            log.error("[DateUtil.stringDateToZonedDate] => {}", e.getMessage());
            return null;
        }
    }
}
