package com.mubarok.testcase.utils;

import java.util.List;

public class ObjectUtil {
    @SuppressWarnings("unchecked")
    public static <T extends List<?>> T cast(Object obj) {
        return (T) obj;
    }

}
