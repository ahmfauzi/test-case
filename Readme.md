# Test Case

## Configuration
1. Atur konfigurasi pada ```application.properties```
2. Sesuaikan konfigurasi database, email, dan twilio sesuai kebutuhan anda.
3. ```config.cron.expression``` pada ```application.properties``` merupakan cron expression yang digunakan sebagai waktu pengiriman notifikasi.
4. ```config.broadcast.body``` pada ```application.properties``` merupakan body (text) notifikasi.

## Curl
1. Login.
```Login
curl --location --request POST 'http://localhost:9090/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "password" : "admin123",
    "username" : "jun.misugi"
}'
```
2. All subscriber
```all subs
curl --location --request GET 'http://localhost:9090/api/subscriber' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqdW4ubWlzdWdpIiwicm9sZXMiOiJST0xFX1NVUEVSX0FETUlOIiwiZXhwIjoxNjcxNDUwOTM5LCJpYXQiOjE2Mzk5MTQ5Mzl9.XsEsNt-e0a_g2b2F8RN2aFzYo9aQXv9XZ8kZq1bAlG4'
```
3. Add subscriber
```add
curl --location --request POST 'http://localhost:9090/api/subscriber' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqdW4ubWlzdWdpIiwicm9sZXMiOiJST0xFX1NVUEVSX0FETUlOIiwiZXhwIjoxNjcxNDUwOTM5LCJpYXQiOjE2Mzk5MTQ5Mzl9.XsEsNt-e0a_g2b2F8RN2aFzYo9aQXv9XZ8kZq1bAlG4' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name" : "test",
    "email" : "test@yopmail",
    "phone" : "0812192192912",
    "bcType" : "EMAIL"
}'
```
3. Edit Subscriber
```edit subs
curl --location --request PUT 'http://localhost:9090/api/subscriber/3' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqdW4ubWlzdWdpIiwicm9sZXMiOiJST0xFX1NVUEVSX0FETUlOIiwiZXhwIjoxNjcxNDUwOTM5LCJpYXQiOjE2Mzk5MTQ5Mzl9.XsEsNt-e0a_g2b2F8RN2aFzYo9aQXv9XZ8kZq1bAlG4' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name" : "test",
    "email" : "test@yopmail",
    "phone" : "0812192192912",
    "bcType" : "SMS"
}'
```
4. Detail Subscriber
```detail subs
curl --location --request GET 'http://localhost:9090/api/subscriber/3' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqdW4ubWlzdWdpIiwicm9sZXMiOiJST0xFX1NVUEVSX0FETUlOIiwiZXhwIjoxNjcxNDUwOTM5LCJpYXQiOjE2Mzk5MTQ5Mzl9.XsEsNt-e0a_g2b2F8RN2aFzYo9aQXv9XZ8kZq1bAlG4'
```

5. Delete Subscriber
```delete subs
url --location --request DELETE 'http://localhost:9090/api/subscriber/3' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqdW4ubWlzdWdpIiwicm9sZXMiOiJST0xFX1NVUEVSX0FETUlOIiwiZXhwIjoxNjcxNDUwOTM5LCJpYXQiOjE2Mzk5MTQ5Mzl9.XsEsNt-e0a_g2b2F8RN2aFzYo9aQXv9XZ8kZq1bAlG4'
```

6. Report
```report
curl --location --request GET 'http://localhost:9090/api/report?startDate=2021-12-18&endDate=2021-12-11&type=SMS&status=FAILED' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqdW4ubWlzdWdpIiwicm9sZXMiOiJST0xFX1NVUEVSX0FETUlOIiwiZXhwIjoxNjcxNDUwOTM5LCJpYXQiOjE2Mzk5MTQ5Mzl9.XsEsNt-e0a_g2b2F8RN2aFzYo9aQXv9XZ8kZq1bAlG4' \
--data-raw ''
```